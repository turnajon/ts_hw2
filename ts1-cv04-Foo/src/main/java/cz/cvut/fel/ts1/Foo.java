package cz.cvut.fel.ts1;

public class Foo {

    private int num = 0;

    public int returnNumber() {
        return 5;
    }

    public int getNum() {
        return num;
    }

    public void increment() {
        num++;
    }
    public boolean isTrue(boolean logicValue){
        return logicValue;
    }

    public void exceptionThrown() throws Exception {
        throw new Exception("Ocekavana vyjimka");
    }

}
