package lab003;

import cz.cvut.fel.ts1.Foo;
import org.junit.jupiter.api.*;

public class FooTests {

    private Foo foo;
    @BeforeEach
    public void testSetup(){
        foo = new Foo();
    }

    @Test
    @DisplayName("foo.returnNumber()")
    public void assertNumber5_ReturnsNumber_Number(){
        Assertions.assertEquals(5, foo.returnNumber());
    }
    @Test
    @Order(2)
    @DisplayName("foo.getNum()")
    public void getNum0_ReturnsNumber_Number(){
        Assertions.assertEquals(0,foo.getNum());
    }
    @Test
    public void isTrue_ReturnsBoolean_Boolean(){
        Assertions.assertTrue(foo.isTrue(true));
    }

    @Test
    @Disabled
    public void failedIsTrue_ReturnsBoolean_Boolean(){
        Assertions.assertFalse(foo.isTrue(true));
    }
    @Test
    public void exceptionThrown_ReturnsException_Exception(){
        Assertions.assertThrows(Exception.class, () -> foo.exceptionThrown());
    }
    @AfterEach
    public void cleanEnvironment(){
        System.out.println("cleanEnvironment was called");
    }
}
