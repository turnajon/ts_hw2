package lab003;

import cz.cvut.fel.ts1.CalculatorTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculatorTests {
    private CalculatorTest calc;
    @BeforeEach
    public void testSetup(){
        calc = new CalculatorTest();
    }

    @Test
    public void add_ReturnsNumber_Number(){
        Assertions.assertEquals(5, calc.add(3,2));
    }
    @Test
    public void subtract_ReturnsNumber_Number(){
        Assertions.assertEquals(5, calc.subtract(8,3));
    }
    @Test
    public void multiply_ReturnsNumber_Number(){
        Assertions.assertEquals(10, calc.multiply(5,2));
    }
    @Test
    public void divide_ReturnsNumber_Number(){
        Assertions.assertEquals(5, calc.divide(10,2));
    }

    @Test
    public void failedDivide_ReturnsNumber_Number(){
        Assertions.assertThrows(Exception.class, () ->calc.divide(5,0));
    }
}
